defmodule RentBotWeb.Tasks.Imovirtual do
  require Logger

  def import_ads(page \\ 1) do
    Logger.info("Checking Imovirtual for updates...")
    case Crawler.Imovirtual.import(page) do
      [] ->
        :stop
      entries ->
        new_entries =
          entries
          |> Enum.map(&insert_entry/1)
          |> Enum.filter(fn x -> x != nil end)

        if (length(new_entries) > 0) do
          subscribers = RentBot.Subscribers.list_subscribers()
          Enum.each(new_entries, fn entry ->
            Enum.each(subscribers, fn x ->
              RentBotWeb.BotController.send_card(x.psid, entry)
            end)
          end)
          import_ads(page + 1)
        end
    end
  end

  defp insert_entry(entry) do
    case RentBot.Ads.get_ad_by_url(entry.url) do
      nil -> RentBot.Ads.create_ad(entry)
      _other -> nil
    end
  end
end
